﻿using cma_officer.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace cma_officer.Services
{
    public class MyService
    {
        private readonly IHttpClientFactory _clientFactory;

        public IEnumerable<WeatherForecast> Branches { get; set; }



        public MyService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<WeatherForecast> GetWeatherForecasts()
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                "https://www.metaweather.com/");
            request.Headers.Add("Accept", "application/json");


            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<WeatherForecast>(content);
            }
            return null;
        }
    }
}
