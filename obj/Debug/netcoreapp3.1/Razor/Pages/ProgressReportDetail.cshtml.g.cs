#pragma checksum "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\ProgressReportDetail.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "825755d57a048f5bed8e10cbe7b18ec8327dfa4d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(cma_officer.Pages.Pages_ProgressReportDetail), @"mvc.1.0.razor-page", @"/Pages/ProgressReportDetail.cshtml")]
namespace cma_officer.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\_ViewImports.cshtml"
using cma_officer;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\_ViewImports.cshtml"
using cma_officer.Data;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"825755d57a048f5bed8e10cbe7b18ec8327dfa4d", @"/Pages/ProgressReportDetail.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f3f4b5854ea6c3247cedbcd8aa577d840e766bc4", @"/Pages/_ViewImports.cshtml")]
    public class Pages_ProgressReportDetail : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-primary btn-icon-text col-md-4 text-white"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "ProgressReport", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\ProgressReportDetail.cshtml"
  
ViewData["Title"] = "ProgressReportDetail";
Layout = "~/Pages/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n<div class=\"container\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n      <div class=\"card\">\r\n        <img");
            BeginWriteAttribute("src", " src=\"", 273, "\"", 279, 0);
            EndWriteAttribute();
            BeginWriteAttribute("alt", " alt=\"", 280, "\"", 286, 0);
            EndWriteAttribute();
            WriteLiteral(@">
        <div class=""card-body"">
          <legend>Progress Report Detail</legend>
          <ul class=""list-group"">
            <li class=""list-group-item list-group-item-action card-inverse-info"">
              <h5>PO 1234567</h5>
              <h6>Kerja-kerja pembersihan dan selenggara am.</h6>
            </li>
            <li class=""list-group-item"">
              <p>Tarikh mula arahan kerja (Kontrak PO)
              <p>
              <p class=""text-muted""><i class=""fas fa-calendar text-info mr-2""></i>
                10-Mar-2020
              </p>
            </li>
            <li class=""list-group-item"">
              <p>Delivery Date (Kontrak PO)
              <p>
              <p class=""text-muted""><i class=""fas fa-calendar text-info mr-2""></i>
                10-Mar-2020
              </p>
            </li>
            <li class=""list-group-item"">
              <p>Syarikat
              <p>
              <p class=""text-muted""><i class=""fas fa-building text-info mr-2""></i>");
            WriteLiteral(@"
                Bina Maju Sdn. Bhd
              </p>
            </li>
            <li class=""list-group-item"">
              <p>Tarikh Rancang Mula Kerja (Workplan)
              <p>
              <p class=""text-muted""><i class=""fas fa-calendar text-info mr-2""></i>
                15-Mar-2020
              </p>
            </li>
            <li class=""list-group-item"">
              <p>Maklumat Lain
              <p>
              <p class=""text-muted""><i class=""fas fa-sticky-note text-info mr-2""></i>
                Limited access due to lockdown
              </p>
            </li>
            <li class=""list-group-item"">
              <p>Lokasi
              <p>
              <p class=""text-muted""><i class=""fas fa-map-marked text-info mr-2""></i>
                PE Abu Bakar Baginda
              </p>
            </li>
            <li class=""list-group-item"">
              <p>Tarikh Penugasan
              <p>
              <p class=""text-muted""><i class=""fas fa-calendar text-i");
            WriteLiteral(@"nfo mr-2""></i>
                10-Mar-2020
              </p>
            </li>
            <li class=""list-group-item"">
              <p>Nama Staff
              <p>
              <p class=""text-muted""><i class=""fas fa-user text-info mr-2""></i>
                A Samad
              </p>
            </li>
            <li class=""list-group-item"">
              <p>Check In Date
              <p>
              <p class=""text-muted""><i class=""fas fa-check-circle text-success mr-2""></i>
                29-Mar-2020
              </p>
            </li>
            <li class=""list-group-item"">
              <p>Check In Date
              <p>
              <p class=""text-muted""><i class=""fas fa-check-circle text-danger mr-2""></i>
                30-Mar-2020
              </p>
            </li>
          </ul>

          

         
         


        </div>
        <div class=""card-footer text-center"">
            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "825755d57a048f5bed8e10cbe7b18ec8327dfa4d7770", async() => {
                WriteLiteral("\r\n              <i class=\"fas fa-arrow-left mr-2\"></i>\r\n              Back\r\n            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<cma_officer.Pages.ProgressReportDetailModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<cma_officer.Pages.ProgressReportDetailModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<cma_officer.Pages.ProgressReportDetailModel>)PageContext?.ViewData;
        public cma_officer.Pages.ProgressReportDetailModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
