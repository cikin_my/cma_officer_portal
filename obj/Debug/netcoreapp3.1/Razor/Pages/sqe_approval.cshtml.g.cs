#pragma checksum "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\sqe_approval.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9144436ea412f879c0f374fbb932f4865e260890"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(cma_officer.Pages.Pages_sqe_approval), @"mvc.1.0.razor-page", @"/Pages/sqe_approval.cshtml")]
namespace cma_officer.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\_ViewImports.cshtml"
using cma_officer;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\_ViewImports.cshtml"
using cma_officer.Data;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9144436ea412f879c0f374fbb932f4865e260890", @"/Pages/sqe_approval.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f3f4b5854ea6c3247cedbcd8aa577d840e766bc4", @"/Pages/_ViewImports.cshtml")]
    public class Pages_sqe_approval : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-danger btn-icon-text col-md-4 text-white btn-md"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-page", "SqeList", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("letter-spacing: 1.5px;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\sqe_approval.cshtml"
  
ViewData["Title"] = "sqe_approval";
Layout = "~/Pages/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""container"">
    <div class=""row"">
    <div class=""col-md-12 grid-margin stretch-card"">
        <div class=""card"">
            <div class=""card-body"">
                <h3 class=""card-title""> <mark class=""bg-success text-white""> PO 1234567</mark></h3>  
                <p class=""card-title"">Kerja-kerja pembersihan dan selenggara am. </p>
          
            <div class=""table-responsive"">
                <table class=""table table-hover table-condense table-sm"">
            
                    <tbody>
                        <tr>
                            <td width=""auto"">Nama Pekerja</td>
                            <td width=""3px"">:</td>
                            <td class=""text-left""> Samad</td>
                        </tr>
                        <tr>
                            <td width=""10%"">lokasi</td>
                            <td width=""3px"">:</td>
                            <td class=""text-left""> PE Abu Bakar Baginda</td>
                        </tr>
   ");
            WriteLiteral(@"                     <tr>
                            <td width=""10%"">Kerja</td>
                            <td width=""3px"">:</td>
                            <td class=""text-left""> Potong Rumput</td>
                        </tr>
                        <tr>
                            <td width=""10%"">Tarikh Hantar SQE</td>
                            <td width=""3px"">:</td>
                            <td class=""text-left""> 12 April 2020</td>
                        </tr>
            
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
    <div class=""col-md-12 grid-margin stretch-card"">
        <div class=""card"">
          <img");
            BeginWriteAttribute("src", " src=\"", 1885, "\"", 1891, 0);
            EndWriteAttribute();
            BeginWriteAttribute("alt", " alt=\"", 1892, "\"", 1898, 0);
            EndWriteAttribute();
            WriteLiteral(@">
          <div class=""card-body"">
            <h5 class=""card-title""> CSQA Assessement</h5>
            <p class=""card-text""></p>
            form goes here
            
          
          </div>
          <div class=""card-footer text-center"">
                  ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9144436ea412f879c0f374fbb932f4865e2608906914", async() => {
                WriteLiteral("\r\n              <i class=\"fas fa-check mr-2\"></i>\r\n              Acknowledge\r\n            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Page = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n          </div>\r\n        </div>\r\n    </div> \r\n\r\n</div>\r\n\r\n    \r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n");
#nullable restore
#line 70 "D:\All PROJECTs\BITBUCKET Clone\cma_officer_local\Pages\sqe_approval.cshtml"
   await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
#nullable disable
                WriteLiteral(@"<script type=""text/javascript"" language=""JavaScript"">

    $.  getJSON (""https://bitbucket.org/cikin_my/myjson/raw/3fc8a93f3814df493a03a6e190df4b448434cfc8/sqe_checklist.json"",
        function (data) {
      
       var items = [];
            $.each(data.active_po, function (key, val) {
                items.push(""<tr>"");
                items.push(""<td id='"" + key + ""'>"" + val.id + ""</td>"");
                items.push(""<td id='"" + key + ""'>"" + val.title + ""</td>"");
                 items.push(""<td id='"" + key + ""'>"" + val.lokasi + ""</td>"");
                  items.push(""<td id='"" + key + ""'>"" + val.startDate + ""</td>"");
                   items.push(""<td id='"" + key + ""'>"" + </td>"");
                items.push(""</tr>"");
            });

            $(""<tbody>"", {html: items.join("""")}).appendTo(""#mytable"");
              
    });

   
</script>
");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<cma_officer.Pages.sqe_approvalModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<cma_officer.Pages.sqe_approvalModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<cma_officer.Pages.sqe_approvalModel>)PageContext?.ViewData;
        public cma_officer.Pages.sqe_approvalModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
