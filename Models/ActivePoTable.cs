﻿using System;
using System.Collections.Generic;

namespace cma_officer.Models
{
    public partial class ActivePoTable
    {
        public int Id { get; set; }
        public string Po { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public DateTime? StartDate { get; set; }
    }
}
