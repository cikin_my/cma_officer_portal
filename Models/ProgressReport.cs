﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cma_officer.Models
{
    public class ProgressReports
    {
        public bodyPayload bosyString { get; set; }
        public string workProgressId { get; set; }
        public string workScheduledId { get; set; }
        public string workProgressNotes { get; set; }
        public DateTime workProgressDateCreated { get; set; }
        public string workScheduledToDoNotes { get; set; }
        public object workScheduledLocation { get; set; }
        public DateTime workScheduledDateScheduled { get; set; }
        public string ponumber { get; set; }
        public string poworkplanId { get; set; }
        public string workerId { get; set; }
        public bool? isCheckedIn { get; set; }
        public DateTime? checkedInDatetime { get; set; }
        public object checkedInLatitude { get; set; }
        public object checkedInLongitude { get; set; }
        public object isCheckedOut { get; set; }
        public object checkedOutDatetime { get; set; }
        public object checkedOutLatitude { get; set; }
        public object checkedOutLongitude { get; set; }
        public DateTime workScheduledDateCreated { get; set; }
        public string poInfoId { get; set; }
        public DateTime podate { get; set; }
        public string contractorNumber { get; set; }
        public string contractorName { get; set; }
        public DateTime deliveryDate { get; set; }
        public string shipToAddress { get; set; }
        public string description { get; set; }
        public string scope { get; set; }
        public double povalue { get; set; }
        public DateTime poInfoDateCreated { get; set; }
        public string username { get; set; }


        public class bodyPayload
        {


            public string Ponumber { get; set; }
            public string WorkProgressMonthString { get; set; }
            public string WorkProgressYearString { get; set; }
        }



    }
}
