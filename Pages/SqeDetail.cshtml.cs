﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;

namespace cma_officer.Pages
{
    [Authorize]
    public class SqeDetailModel : PageModel
    {

        private readonly UserManager<IdentityUser> _userManager;
       
        public string UserId { get; set; }        
        public void OnGet()
        {
            UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

        }
        public void OnPost()
        {
          
        }
    }

    
}
